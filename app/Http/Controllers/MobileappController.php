<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Models\Apimodel;
class MobileappController extends Controller
{    
     
      public function __construct(){
		
		
	  }
     
      public function index(){
		
		
   
      }

	  public function uploadphoto(Request $request){
    $validator = Validator::make($request->all(), [
    'user_id' => 'required',
    'project_id' => 'required',
    'api_app_version' => 'required',
	'platform' => 'required',
	'token'=>'required',
    'file_type'=>'required',
    'image_file'=>'required',
	'photo_date'=>'required',
       
    ]);

  if ($validator->fails()) {
        echo response()->json(['response_code' => '404','message'=>$validator->errors()]);
            
    }else{

	  $files= $request->image_file;
	  $file_count = count($files);
      $uploadCount=0;
      $uploadedImage=array();
      foreach($files as $file){

      $imageName = $uploadCount.time().'.'.$file->getClientOriginalExtension();
      if($file->move(public_path('images'), $imageName)){

      $uploadedImage[]=$imageName;
      $uploadCount++;   
	  
	   }

	  }

       if($uploadCount==$file_count){

	   $Query = Apimodel ::testme($request,$uploadedImage);
	  
	  }else{
	  return response()->json(['response_code' => '404', 'message' => 'Error in processing']); 
	  }



	
    }
    

	  }
    
	 

     public function GetProgressPhotoAndVideo(Request $request){
	 $validator = Validator::make($request->all(), [
    'user_id' => 'required',
    'project_id' => 'required',
    'api_app_version' => 'required',
	'platform' => 'required',
	'token'=>'required',
    'file_type'=>'required',
    'total_count'=>'required',
	'page_no'=>'required',
       
    ]);

    if ($validator->fails()) {
        echo response()->json(['response_code' => '404','message'=>$validator->errors()]);
            
    }else{


     $Query = Apimodel ::getphotosNvideos($request);



	}
	
	
	
  
	  
	  }

 


}
